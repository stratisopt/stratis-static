﻿var mNavbarElements;
var mNavbarCollapsedForm;
var mRightNavbarSpacer;

var mMainLayer;
var mAdornerLayer;
var mIsAdornerLayerVisible;

var mNavMenuContainer;

var mWitbLogoContainer;
var mNavbarCircleGradient;
var mLeftGradientPadding;
var mRightGradientPadding;
var mVirtualCircle;

var mNavGradientContainer;
var mLeftNavSpring;

var mMainPageDimmingLayer;

var mPaddingFromCss = 7.21667; // in pixels

var collapsers = [];

window.addEventListener('DOMContentLoaded', InitializeJavascript);

function InitializeJavascript() {
    InitializeVariables();
    TheMainFunction();
    window.addEventListener('resize', TheMainFunction);
}

function InitializeVariables() {
    mNavbarElements = document.getElementById("nav-elements");
    mNavbarCollapsedForm = document.getElementById("navbar-collapsed-form");

    mRightNavbarSpacer = document.getElementById("right-navbar-spacer");

    mMainLayer = document.getElementById("main-layer");
    mIsAdornerLayerVisible = false;
    mAdornerLayer = document.getElementById("adorner-layer");
    mNavMenuContainer = document.getElementById("nav-menu-container");

    mWitbLogoContainer = document.getElementById("witb-logo-container");
    mNavbarCircleGradient = document.getElementById("navbar-circle-gradient");
    mLeftGradientPadding = document.getElementById("left-gradient-padding");
    mRightGradientPadding = document.getElementById("right-gradient-padding");
    mVirtualCircle = document.getElementById("virtual-circle");
    mCircleGradientContainer = document.getElementById("circle-gradient-container");

    mNavGradientContainer = document.getElementById("nav-gradient-container");
    mLeftNavSpring = document.getElementById("left-nav-spring");

    mMainPageDimmingLayer = document.getElementById("main-page-dimming-layer");

    if (collapsers.length !== 0) return;
    if (mNavbarElements === null) return;
    var navbarCallback = function (collapsed){
      if (!collapsed && mIsAdornerLayerVisible) {
          ToggleNavMenu();
      }
    }
    var collapser = MakeCollapser(mNavbarElements, mNavbarCollapsedForm, navbarCallback);
    if (collapser) {
	      collapsers.push(collapser);
    } else {
	      window.setTimeout(InitializeVariables, 1);
    }

    // if (collapsers.length == 0)  {
    // 	if (mNavbarElements != null) {
    //     var navbarCallback = function (collapsed){
    //       if (!collapsed && mIsAdornerLayerVisible) {
    //           ToggleNavMenu();
    //       }
    //     }
    //
  	//     var collapser = MakeCollapser(mNavbarElements, mNavbarCollapsedForm, navbarCallback);
  	//     if (collapser) {
  	// 	      collapsers.push(collapser);
  	//     } else {
  	// 	      window.setTimeout(InitializeVariables, 1);
  	//     }
    //   }
    // }
  }

function getDocumentWidth() {
    return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
}

function TheMainFunction() { // this is the main loop that's called on resize
    ScaleWitbNavbarLogoAndCircle();
    // Apply collapse checks
    collapsers.map(function (c){return c();});
    // collapsers.map((c) => c());
}

//=========================================
// Custom Code for this WiTB site
//=========================================

function ScaleWitbNavbarLogoAndCircle() {
    if (mWitbLogoContainer == null) return;

    var widthOfGradientCircle = HtmlPixelToFloat(mWitbLogoContainer.clientWidth);
    var d = Math.sqrt(2 * Math.pow(widthOfGradientCircle, 2));
    var r = d / 2;

    var heightOfGradientContainer = HtmlPixelToFloat(window.getComputedStyle(mNavbarCircleGradient).height); // hopefully returns pixels

    var widthOfCircleTransection = d * (Math.sqrt(Math.abs(1 - Math.pow((r - heightOfGradientContainer) / r, 2))));
    mNavbarCircleGradient.style.width = HtmlFloatToPixel(widthOfCircleTransection);

    var newWidthOfPaddings = HtmlFloatToPixel((d - widthOfCircleTransection) / 2 + mPaddingFromCss * 2);
    mLeftGradientPadding.style.width = newWidthOfPaddings; //offset to make the container hit the side of the screen as if the imaginary circle actually existed.
    mRightGradientPadding.style.width = newWidthOfPaddings;
    mRightNavbarSpacer.style.width = newWidthOfPaddings;

    r = HtmlFloatToPixel(r);
    d = HtmlFloatToPixel(d);

    mNavbarCircleGradient.style.backgroundImage = ("radial-gradient(circle " + r + " at center " + r + " , transparent 99.9999%, rgba(38, 27, 2, .9) 0%");
    mNavbarCircleGradient.style.backgroundColor = "transparent";
    mVirtualCircle.style.width = d;
    mVirtualCircle.style.height = d;
}

//=========================================
// Detecting and Collapsing Navbar Space
//=========================================

function MakeCollapser(element, collapsedElement, collapsedCallback) {
    if (element.offsetParent == null) return false;

    element.style.overflowX = "visible";
    element.style.whiteSpace = "nowrap";
    element.style.flexFlow = "row";

    var ownHeight = element.offsetHeight;

    if (ownHeight == 0) {
	     return false;
    }

    element.style.overflowX = null;
    element.style.whiteSpace = null;
    element.style.flexFlow = null;

    let collapsed = false;
    let trigger = null;

    return function (){
    	var initialState = collapsed;
    	if (getDocumentWidth() > trigger && collapsed) {
    	    trigger = null;
    	    CollapseElement(collapsedElement, element);
    	    collapsed = false;
	       }

    	if (element.offsetHeight > (ownHeight + 1) && !collapsed) {
    	    trigger = getDocumentWidth();
    	    CollapseElement(element, collapsedElement);
    	    collapsed = true;
    	}

    	if (initialState !== collapsed) {
    	    collapsedCallback(collapsed);
    	}
    };
}

function CalculateHorizontalSpaceOfChildren(theParentContainer) {
    if (theParentContainer === null) return 0.0;

    var theChildrenNodeList = theParentContainer.children;
    var spaceOfChildren = 0.0; // seems like I have to define or type cast this in some way before it hits the loop below? Not sure why this fixes the 'undefined' problem.

    for (var i = 0; i < theChildrenNodeList.length; i++) {
        var theElement = theChildrenNodeList[i];

        var theElementAttributes = window.getComputedStyle(theElement);
        var elementLeftMargin = HtmlPixelToFloat(theElementAttributes.marginLeft);
        var elementRightMargin = HtmlPixelToFloat(theElementAttributes.marginRight);

        var elementWidth = HtmlPixelToFloat(theElementAttributes.width);

        spaceOfChildren += (elementWidth + elementLeftMargin + elementRightMargin);
    }

    return spaceOfChildren;
}

function CollapseElement(elementToCollapse, collapsedVersion) {
    // might simply do hide and show for now, but in actuality, there are probably advantages of pulling and pushing elements to the dom.
    HideElement(elementToCollapse);
    ShowElement(collapsedVersion);
}


function ToggleNavMenu() { //I'm making this the entire adorner-layer for now, but that's not strictly always going to be the case, if I use this layer for a bunch of other stuffs.
    var animationTimeMilliSec = 250;
    if (mIsAdornerLayerVisible === false) {
        ShowElement(mAdornerLayer);
        ExecuteAnimation(mNavMenuContainer, "top-right-swoop", animationTimeMilliSec);
        mMainLayer.classList.add('so-backgroundify-to-viewport');
        mIsAdornerLayerVisible = true;
        return;
    }
    ReverseAnimation(mNavMenuContainer, "top-right-swoop", animationTimeMilliSec);
    DelayedHide(mAdornerLayer, animationTimeMilliSec);
    setTimeout(function anon(){mMainLayer.classList.remove('so-backgroundify-to-viewport');}, animationTimeMilliSec);
    mIsAdornerLayerVisible = false;
    return;
}
function DelayedHide(theElement, timeInMilliSec) {
    setTimeout(function anon() { HideElement(theElement) }, timeInMilliSec); //time needs to be same as the animation
}


//=========================
// Animation Functions
//=========================

function ExecuteAnimation(theElement, animationName, timeInMilliSec) {
    CreateAnimationCSS(animationName, timeInMilliSec);
    theElement.classList.add("animation-pre-req");
    setTimeout(function anon() { theElement.classList.add('animation-start'); }, 25); // have to add a very slight delay so that this will work will elements that have just become visible through display property
}

function ReverseAnimation(theElement, animationName, timeInMilliSec) {
    CreateAnimationCSS(animationName, timeInMilliSec);
    theElement.classList.remove("animation-start");
    setTimeout(function anon() { theElement.classList.remove('animation-pre-req'); }, timeInMilliSec);
}

function CreateAnimationCSS(animationName, timeInMilliSec) {
    var timeInSec = MilliSecToSec(timeInMilliSec);
    var animationPreReqString = "";
    var animationExecuteString = "";
    var css = document.createElement("style");
    if (animationName === "top-right-swoop") // Only doing the one for now, but you could make a whole list for direction and type of curve, or type of it's even a fade-in style at all
    {
        animationPreReqString += "transform: translate(100%,-100%) scale(0,0);";
        animationPreReqString += "transition: all cubic-bezier(.65,.05,.36,1) " + timeInSec + "s" + ";";
        animationExecuteString = "transform: translate(0%, 0%) scale(1,1) !important;"
    }
    else {
        // Probably want a default animation here.
    }
    animationExecuteString = ".animation-start { " + animationExecuteString + " } ";
    animationPreReqString = ".animation-pre-req { " + animationPreReqString + " } ";
    css.innerHTML = animationPreReqString + " " + animationExecuteString;
    document.head.appendChild(css);
    CleanUpGeneratedCSS(css, timeInMilliSec);
}
function CleanUpGeneratedCSS(theCSS, timeInMilliSec) {
    setTimeout(function anon() { document.head.removeChild(theCSS); }, timeInMilliSec);
}


//=========================
// Helper Functions
//=========================

function HtmlPixelToFloat(aString) { // super non-generic for now.
    if (aString === null) return 0.0;
    var floatToReturn = parseFloat(aString);

    return floatToReturn;
}

function HtmlFloatToPixel(aFloat) { // this needs to do converstions to make it safe. Some normalization. It's dumb for now, yo.
    if (aFloat === null) return "";
    return aFloat += "px";
}

function MilliSecToSec(timeInMilliSec) { // not generic. Could create a generic conversion set of functions later, that convert to a common type first, and then can go in any direction.
    return timeInMilliSec / 1000;
}

function HideElement(elementToHide) {
    elementToHide.classList.add('so-hide');
}
function ShowElement(elementToShow) {
    elementToShow.classList.remove('so-hide');
}


//======================================
// Rando internet copy code, cleaned up
//======================================

function CopyToClipboard(text) {
    if (window.clipboardData && window.clipboardData.setData)
        return clipboardData.setData("Text", text); // IE specific code path to prevent textarea being shown while dialog is visible.

    if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
        var textarea = document.createElement("textarea");
        textarea.textContent = text;
        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
        textarea.style.width = "1px";
        textarea.style.height = "1px";
        document.body.appendChild(textarea);
        textarea.select();
        try {
            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
        }
        catch (ex) {
            console.warn("Copy to clipboard failed.", ex);
            return false;
        }
        finally {
            document.body.removeChild(textarea);
        }
    }
}


//======================================
// My crappy code for copying
//======================================

function CopyAddressToClipboard(addressString) {
    var chasingCopyButton = document.getElementById("copy-button");
    var chasingCopyButtonText = document.getElementById("copy-button-text");
    var textToShow = "Copied Text";
    if (CopyToClipboard(addressString) === true) {
        chasingCopyButtonText.textContent = " ";
        setTimeout(ShowTextAgainFunction(chasingCopyButtonText, textToShow), 500); // this timeout needs to be the same as the animation length
        chasingCopyButton.classList.add("copy-address-alt-text-size");
        chasingCopyButton.classList.add("so-unselectable");
        setTimeout(RevertContactAddressButton, 3000);
        return true;
    }
    chasingCopyButton.textContent = "Failed to Copy Text"; // a note, that I didn't really test for errors in browers returning the wrong thing. It will still work correctly and safely, but the width of the text in the display will be wrong, and will stay wrong, since I'm not reverting it back on a failure.
    return false;
}

function RevertContactAddressButton() {
    var chasingCopyButton = document.getElementById("copy-button");
    var chasingCopyButtonText = document.getElementById("copy-button-text");
    var textToShow = "Copy Email Address to Clipboard"

    chasingCopyButtonText.textContent = " ";
    chasingCopyButton.classList.remove("copy-address-alt-text-size");
    chasingCopyButton.classList.remove("so-unselectable");
    setTimeout(ShowTextAgainFunction(chasingCopyButtonText, textToShow), 500); // this timeout needs to be the same as the animation length
}

function ShowTextAgainFunction(domTextElement, textToShow) {
    return function ShowTextAgain() { domTextElement.textContent = textToShow; }
}
