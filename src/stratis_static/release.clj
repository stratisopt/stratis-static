(ns stratis-static.release
  (:require [stratis-static.dev :as dev]
            [me.raynes.fs :as fs]
            [clojure.java.io :as io]
            [stratis-static.file-helpers :as fh]
            [cljs.build.api :as cljs]))

(defmacro build-pages [pages]
  `(do
     (fs/delete-dir "target")
     (fh/copy-stratis-resources)
     (fh/copy-project-resources)
     (cljs/build (-> (:file (meta #'~pages)) io/resource)
                 {:output-to "target/public/cljs-out/dev-main.js"
                  :optimizations :advanced})
     (dev/set-pages ~pages)
     (dev/render-all)))
 
