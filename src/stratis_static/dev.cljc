(ns stratis-static.dev
  #?(:cljs (:require-macros [hiccups.core :as hiccup :refer [html]]
                            [stratis-static.macros :as macros]))
  (:require #?(:clj [hiccup.core :as hiccup :refer [html]])
            #?(:clj [clojure.java.io :refer [writer make-parents delete-file resource file]])
            #?(:clj [stratis-static.macros :as macros])
            #?(:clj [me.raynes.fs :as fs])
            #?(:clj [figwheel.main :as figwheel])
            [stratis-static.helpers :as helpers]
            #?(:clj [stratis-static.file-helpers :as fh])
            #?(:cljs [hiccups.runtime :as hiccupsrt])))

(def pages (atom nil))
(defn set-pages [new-pages]
  (reset! pages new-pages))

(defn unpack-resources []
  #?(:clj (fh/copy-stratis-resources)))

(defn generate-html [page-info]
  (html
   [:html
    (helpers/hiccup-merge
     [:head
      [:meta {:charset "UTF-8"}]
      [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
      [:link {:href "/simple_CSS_framework.css" :rel "stylesheet" :type "text/css"}]
      [:script {:type "text/javascript" :src "/StatisLibrary.js"}]]
     (:head page-info))
    (helpers/hiccup-merge
     (helpers/hiccup-merge [:body] (:body page-info))
     [:script {:type "text/javascript" :src "/cljs-out/dev-main.js"}])]))

;; Define two renderers, one on the server and one on the client.
;; It would make sense to break this into it's own file...
;; Especially if we start including some compiled js.
(defn render [page-info]
  #?(:cljs
     ;; This is acceptable for dev, know that you'll have to re-render the index from lein for some changes.
     ;; DO NOT DO THIS ON THE PRODUCTION PAGE.
     (set! (-> js/document .-body .-innerHTML)
           (html (:body page-info)))
     :clj
     (do
       (unpack-resources)
       (make-parents (str "target/public/" (:path page-info)))
       (with-open [w (writer (str "target/public/" (:path page-info)))]
         (do
           (.write w "<!DOCTYPE html>\n")
           (.write w (generate-html page-info)))))))

#?(:cljs
   (defn reload-body []
     (let [window-location (-> js/window .-location .-pathname)
           location (if (or (nil? window-location)
                            (= "" window-location)
                            (= "/" window-location))
                      "/index.html"
                      window-location)]
       (if-let [matching (some
                          #(when (= (:path %) location) %) @pages)]
         (render matching)))))

;; Go ahead and render once this file is loaded.
;; This way your REPL session will have fresh pages.
(defn render-all [& args]
  #?(:clj 
     (doall (map render @pages))
     :cljs nil))

(defn on-js-reload []
  #?(:cljs
     (do
       (.log js/console "Reloading body")
       (reload-body)
       (js/InitializeJavascript))
     :clj
     (render-all)))

(defonce hook-reload
  (add-watch pages :reload (fn [& args] (on-js-reload))))

(render-all)

