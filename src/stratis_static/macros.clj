(ns stratis-static.macros
  "Functions useful to lift into macro-space for serializing files into the javascript bundle"
  (:require [clojure.tools.reader.edn :as edn]
            [clojure.java.io :as io]
            [me.raynes.fs :as fs]))

(defmacro aot-slurp [file]
  (if (fs/exists? file)
    (clojure.core/slurp file)
    (throw (AssertionError. (str file " does not exist and is required")))))

(defmacro aot-seq [file]
  (if (fs/exists? file)
    (with-open [rdr (clojure.java.io/reader file)]
      (into [] (line-seq rdr)))
    (throw (AssertionError. (str file " does not exist and is required")))))
