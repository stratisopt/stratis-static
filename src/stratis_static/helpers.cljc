(ns stratis-static.helpers)

(defn hiccup-merge [target elements]
  (if (keyword? (first elements))
    (conj target elements)
    (into target elements)))

