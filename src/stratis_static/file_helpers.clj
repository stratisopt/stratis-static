(ns stratis-static.file-helpers
  (:require [me.raynes.fs :as fs]
            [cljs.build.api :as cljs]
            [clojure.java.io :as io]))

(defn itteration-to-files [[parent-file sub-directory-set file-set]]
  (into [] (map #(str parent-file (java.io.File/separator) %) file-set)))

(defn graft-file-name [target target-root destination-root]
  (let [c-target (str (fs/normalized target))
        c-target-root (str (fs/normalized target-root))
        c-destination-root (str (fs/normalized destination-root))]
    (clojure.string/replace c-target c-target-root c-destination-root)))

(defn merge-dir [directory destination]
  (doall (->>
          (mapcat itteration-to-files (fs/iterate-dir directory))
          (map #(fs/copy+ % (graft-file-name  % directory destination))))))

(defn copy-stratis-resources []
  (merge-dir (-> "stratis-public" io/resource io/file) "target/public"))

(defn copy-project-resources []
  (merge-dir (-> "public" io/resource io/file) "target/public"))

